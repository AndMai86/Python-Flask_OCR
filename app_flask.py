from flask import Flask, jsonify
import re
import pdfplumber

app = Flask(__name__)
app.config['DEBUG'] = True


pdfFileName = 'file_pdf/BUSTA_PAGA_AM.pdf'
# dictionary definition
citPaycheck = {
    "COMPANY": "",  # company name
    "COMPANY_ADDRESS": "",
    "TAX_CODE_COMPANY": "",
    "EMPLOYEE": "",
    "EMPLOYEE_ADDRESS": "",
    "TAX_CODE_EMPLOYEE": "",
    "TOT_SALARY": ""
}
retribuzione_ordinaria_dict = {
    "ore_gg_num": "",
    "val_u_neutra": "",
    "competenze": "",
    "trattenute": ""

}
gratifica_natalizia_dict = {
    "ore_gg_num": " ",
    "val_u_neutra": "",
    "competenze": "",
    "trattenute": ""

}

retributivo_settore_dict = {
    "ore_gg_num": "",
    "val_u_neutra": "",
    "competenze": "",
    "trattenute": ""

}
festivita_godute_dict = {
    "ore_gg_num": "",
    "val_u_neutra": "",
    "competenze": "",
    "trattenute": ""

}
buoni_pasto_dict = {
    "ore_gg_num": "",
    "val_u_neutra": "",
    "competenze": "",
    "trattenute": ""

}

info_fiscale_dict = {
    "imponibile_irpef_lorda": "",
    "imp_tas_separata": "",
    "imponibile_arretrati_ap": "",
    "add_regionale_ap": "",
    "add_comunale_ap": "",
    "trattenuta_irpef_lorda": "",
    "trattenuta_tas_separata": "",
    "aliquota_media_ap": "",
    "add_regionale_ac": "",
    "add_comunale_ac": "",
    "detrazioni": "",
    "trattenuta_irpef_netta": "",
    "trattenuta_arretr_ap": "",
    "add_comunale_acconto": ""

}

valori_TOTALE_dict = {

    "TOTALE COMPETENZE": "",
    "TOTALE TRATTENUTE": "",
    "TOTALE TRATT.SOCIALI": "",
    "TOTALE TRATT.FISCALE": "",
    "CONGUAGLIO IRPEF": "",
    "ARROTOND.PRECEND": "",
    "ARROTOND.ATTUALE": ""
}
contrib_valori_dict = {
    "imponibile_fpld": "",
    "ctr_fpld": "",
    "ctr_cigs": ""
}
progr_dict = {
    "imp_inail": "",
    "imp_contr": "",
    "contributi": "",
    "reddito_compl": "",
    "oneri_deducibili": "",
    "imp_irpef": "",
    "detrazioni_lavoro_dip": "",
    "detrazioni_familiari": ""
}


@app.route("/")
def hello():
  return "Hello FLASK!"

@app.errorhandler(404)
def invalie_route(e):
    #'route errata!'
    return jsonify({'errorCode':404,'message':'route errata!'})

def last_pos_NO_empty(valuesString):
    stringLen = len(valuesString)
    if(len(valuesString) ==4):
        if valuesString[3] == '' and (valuesString[1] != '' or valuesString[2] !=''):
            if valuesString[2] == '' and valuesString[1] != ''  :
                res = 1
            else:
                res = 2
        elif valuesString[3] != '' :
            res = 3
        elif valuesString[2] == '':
            res = 1
        else:
            res = 2
    elif len(valuesString) == 3:
        if valuesString[2] == '' and (valuesString[0] != '' or valuesString[1] !=''):
            if valuesString[2] == '' and valuesString[1] != ''  :
                res = 1
            else:
                res = 2

        elif valuesString[2] == '':
            res = 1
        else:
            res = 2

    return res


def has_numbers(inputString):
    x = re.search("re.search(r'\d+')", inputString)
    if x:
        #print("YES! are numbers")
        res = True
    else:
        res = False
    return res

def no_empty_values(original_list):
    list_no_empty = []
    for string in original_list:
        if (string != ""):
            list_no_empty.append(string)
    return list_no_empty

@app.route('/file_pdf/<pdf_name>')
def get_extracted(pdf_name):
    path_files='./file_pdf/'
    pdfFileName = path_files+pdf_name+'.pdf'
    text = ''
    with pdfplumber.open(pdfFileName) as pdf:
        main_page = pdf.pages[0]
        text = main_page.extract_text()

    nghezza_3_slot = 66
    lunghezza_1_slot = 46

    pattern_nome_ditta = '                                  '
    pattern_indirizzo_ditta = re.compile('                 ')
    pattern_dipendente = re.compile('D IPE N DE NT E(.+)ELEMENTI DELLA RETRIBUZIONE')
    text_nome_ditta = text.split("\n")[0]
    text_cod_fiscale_ditta = text.split("\n")[3]
    text_dipendente = text.split("\n")[4]
    text_indirizzo_dipendente = text.split("\n")[5]
    text_indirizzo_citta = text.split("\n")[1]  # -----
    text_cod_fiscale_dipendente = text.split("\n")[8]
    text_totale_stipendio = text.split("\n")[17]
    text_retribuzione_ordinaria = text.split("\n")[19]  # fatto

    text_gratifica_natalizia = text.split("\n")[20]
    text_retributivo_settore = text.split("\n")[21]
    text_festivita_godute = text.split("\n")[23] #qui
    text_buoni_pasto = text.split("\n")[24]
    print('#####################  festivita_godute  ###################')
    print(text_festivita_godute)
    text_irpef_lorda_detrazioni_1 = text.split("\n")[28]
    text_tassazione_separata_irpef_netta = text.split("\n")[29]
    text_imponibile_tratt_ap = text.split("\n")[30]
    text_add_regionale_ap_ac = text.split("\n")[32]  # [31]
    text_irpes_annuo_pagata = text.split("\n")[35]
    text_arrotond_preced = text.split("\n")[38]
    text_arrotond_attuale = text.split("\n")[40]
    # print('Riga add_regionale_ap:')
    # print(text_add_regionale_ap_ac)
    add_regio_ap = text_add_regionale_ap_ac[34:45]
    print('add_regio_ap:')
    print(add_regio_ap)

    add_regio_ac = text_add_regionale_ap_ac[63:64]
    print('add_regio_ac')
    print(add_regio_ac)
    text_add_comunale = text.split("\n")[33]
    add_comu_ap = text_add_comunale[5:25]

    print(text_add_comunale[4:43])  # [3:25]

    if (has_numbers(add_regio_ap)):
        info_fiscale_dict["add_regionale_ap"] = add_regio_ap
    else:
        info_fiscale_dict["add_regionale_ap"] = '        '
    if (has_numbers(add_regio_ac)):
        info_fiscale_dict["add_regionale_ac"] = add_regio_ac
    else:
        info_fiscale_dict["add_regionale_ac"] = '        '

    text_comun = text_add_comunale.split()


    # print(text_add_comunale)
    x = re.search("(.{25})(\s*|\d+)(0-9)*(\s+|\d+)", text_add_comunale).group(4)  ########## ACCONTO(\.+')\S+
    print('ACCONTO: ' + x)  #
    if (text_comun[2].isnumeric()):  # ADD.COMUNALE A.P
        info_fiscale_dict['add_comunale_ap'] = text_comun[2]
    else:
        info_fiscale_dict['add_comunale_ap'] = '     '

    if (text_comun[5].isnumeric()):  # ADD.COMUNALE A.C
        info_fiscale_dict['add_comunale_ac'] = text_comun[5]
    else:
        info_fiscale_dict['add_comunale_ac'] = '     '

    imp_arr_ap = text_imponibile_tratt_ap[37:45]
    aliquota_media_ap = text_imponibile_tratt_ap[78:88]
    trattenuta_arretr_ap = text_imponibile_tratt_ap[122:130]  # ===================================
    if has_numbers(trattenuta_arretr_ap):
        info_fiscale_dict["trattenuta_arretr_ap"] = trattenuta_arretr_ap
    else:
        info_fiscale_dict["trattenuta_arretr_ap"] = '        '

    print('- trattenuta_arretr_ap' + trattenuta_arretr_ap)
    info_fiscale_dict["aliquota_media_ap"] = aliquota_media_ap

    ###########aliquota_media_ap = re.search('AAA(.+?)ZZZ', text).group(1)
    # print('lunghezza retribuzione_ordinaria text: '+str(len(text_retribuzione_ordinaria)))    ##info_fiscale = {
    # print('11--111 '+text_irpef_lorda_detrazioni_1)
    print('22--222 ' + text_tassazione_separata_irpef_netta)
    irpef_lorda__1 = text_irpef_lorda_detrazioni_1[0:45]
    irpef_lorda__1 = irpef_lorda__1.split('\d+')[0]
    irpef_lorda__1 = re.split('   ', irpef_lorda__1)

    tassazione_irpef_lorda_1 = text_irpef_lorda_detrazioni_1[50:90]
    tassazione_irpef_lorda_1 = tassazione_irpef_lorda_1.split('\d+')[0]
    tassazione_irpef_lorda_1 = re.split('   ', tassazione_irpef_lorda_1)
    detrazioni_1 = text_irpef_lorda_detrazioni_1[115:]

    imp_tass_separata = text_tassazione_separata_irpef_netta[0:45]
    tratt_tassaz_separata = text_tassazione_separata_irpef_netta[48:90]
    tass_separata_irpef_netta = text_tassazione_separata_irpef_netta[90:135]  #################
    #   tratt_irpef_netta

    if (has_numbers(tratt_tassaz_separata)):
        tratt_tassaz_separata = re.split(r'\d+', tratt_tassaz_separata)
    else:
        tratt_tassaz_separata = ' '

    info_fiscale_dict["trattenuta_tas_separata"] = tratt_tassaz_separata
    # print('splitting tratt_tassaz_separata')
    print(tratt_tassaz_separata)

    info_fiscale_dict["imp_tas_separata"] = tass_separata_irpef_netta[3]
    # print('tratt_tassaz_separata')
    # print(tratt_tassaz_separata)

    trattenuta_irpef_lorda_1 = text_irpef_lorda_detrazioni_1[50:90]
    trattenuta_irpef_lorda_1 = trattenuta_irpef_lorda_1.split('\d+')[0]
    trattenuta_irpef_lorda_1 = re.split('   ', trattenuta_irpef_lorda_1)

    detrazioni_1 = text_irpef_lorda_detrazioni_1[115:]
    print('@@@@@@@@@ tass_separata_irpef_netta')
    print(tass_separata_irpef_netta)
    tratt_irpef_netta = re.split('   ', tass_separata_irpef_netta)
    ##########tratt_irpef_netta = tratt_irpef_netta[1] corretta tass_separata_irpef_netta

    print(tratt_irpef_netta)
    info_fiscale_dict["trattenuta_irpef_netta"] = tratt_irpef_netta
    info_fiscale_dict['"imponibile_arretrati_ap"'] = imp_arr_ap

    #################################tassaz.separata-irpef_netta = text_tassazione_separata_irpef_netta[]
    info_fiscale_dict['trattenuta_irpef_lorda'] = trattenuta_irpef_lorda_1[0]  #[1]  corretta tass_separata_irpef_netta
    print('xxxOOO irpef_lorda__1')
    print(irpef_lorda__1)
    #################################### info_fiscale_dict["imponibile_irpef_lorda"] = irpef_lorda__1[1]###[2]
    info_fiscale_dict['detrazioni'] = detrazioni_1

    pattern_indirizzo_dipendente = 'Minimo'  # re.split('Minimo', text_indirizzo_dipendente)
    # pattern_codice_fiscale_dipendente = '[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}'#'COD. FISCALE'
    # nome_ditta = re.split(pattern_nome_ditta,text_nome_ditta)[0]  #whiteSpaceRegex
    nome_deipendente = re.split(pattern_dipendente, text_dipendente)
    indirizzo_ditta = re.split(pattern_indirizzo_ditta, text_indirizzo_citta)
    indirizzo_dipendente = re.split(pattern_indirizzo_dipendente, text_indirizzo_dipendente)[0]
    alpha_indirizzo_dipendente = re.sub(r'[^a-zA-Z]', '', indirizzo_dipendente)
    codice_fiscale_ditta = re.split(r'POSIZIONE INPS', text_cod_fiscale_ditta)  # r'[^a-zA-Z]', '', indirizzo_dipendente
    codice_fiscale_dipendente = text_cod_fiscale_dipendente[26:]
    buoni_pasto = re.split(r'             ', text_buoni_pasto)



    print(buoni_pasto)

    #print('text_buoni_pasto')
    #print(buoni_pasto[1])

    citPaycheck["COMPANY"] = re.split(pattern_nome_ditta, text_nome_ditta)[0]

    citPaycheck["COMPANY_ADDRESS"] = re.split(pattern_indirizzo_ditta, text_indirizzo_citta)
    tax_code_company_text = re.split(r'POSIZIONE INPS', text_cod_fiscale_ditta)
    print('TAX_CODE_COMPANY    len array')
    print(str(tax_code_company_text))
    #citPaycheck["TAX_CODE_COMPANY"] = re.split(r'POSIZIONE INPS', text_cod_fiscale_ditta)
    citPaycheck["EMPLOYEE"] = re.split(pattern_dipendente, text_dipendente)
    citPaycheck["EMPLOYEE_ADDRESS"] = re.sub(r'[^a-zA-Z]', '', indirizzo_dipendente)
    citPaycheck["TAX_CODE_EMPLOYEE"] = text_cod_fiscale_dipendente[25:]

    citPaycheck["TOT_SALARY"] = re.findall('[\d]{4}[?\,][\d]+', text_totale_stipendio)  # [0]

    # print('**add_comunale_ap:**'+text_add_comunale)

    splitted_codice_fiscale_ditta = codice_fiscale_ditta[0].split('  ')
    for i in splitted_codice_fiscale_ditta:
        if (len(i) == 11 and i.isnumeric()):
            print('codice fiscale ditta: ' + i)
            citPaycheck["TAX_CODE_COMPANY"] = citPaycheck["TAX_CODE_COMPANY"]+i

    stipendio = re.findall('[\d]{4}[?\,][\d]+', text_totale_stipendio)
    # print('text_totale_stipendio: '+text_totale_stipendio)
    print('LUNGHEZZA stipendio: ' + str(len(stipendio)))

    retricuzione_ordinaria = re.split(r'             ', text_retribuzione_ordinaria)
    splitted_retribuzione_settore = re.split(r'         ', text_retributivo_settore)  ##################
    print('splitted_retribuzione_settore  buahahhahha')
    print(splitted_retribuzione_settore)

    retributivo_settore_dict["ore_gg_num"] = splitted_retribuzione_settore[1]
    retributivo_settore_dict["val_u_neutra"] = splitted_retribuzione_settore[2]
    retributivo_settore_dict["competenze"] = splitted_retribuzione_settore[2] #era indice 3
    print('CONTROLLO!! retribuzionr_settore'+str(len(splitted_retribuzione_settore)))
    print('text_retributivo_settore')
    print(text_retributivo_settore)
    ##########retricuzione_ordinaria_values = re.split(r'  ', retricuzione_ordinaria[1])
    festivita_godute = re.split(r'              ', text_festivita_godute)  ############## HERE!!!
    print('post slpit festivita_godute')
    print(festivita_godute)
    #####################gratifica_natalizia = re.split(r'                 ',text_gratifica_natalizia)
    splitted_gratifica_natalizia = re.split(r'                 ', text_gratifica_natalizia)
    # print('splitted_retribuzione_settore:')
    # print(text_retributivo_settore)
    # print('splitted_gratifica_natalizia:')
    # print(text_gratifica_natalizia)
    print('-----------------')
    print('-----------------')
    print('-----------------')
    print('CONTROLLSRE LEN DI splitted_gratifica_natalizia')
    print(str(len(splitted_gratifica_natalizia)))
    print('-----------------')
    print('-----------------')
    print('-----------------')
    ##############################gratifica_natalizia_values = splitted_gratifica_natalizia[1].split()

    #RETRIBUZIONE ORDINARIA

    splitted_festivita_godute = re.split(r'         ', text_festivita_godute)  ############## HERE!!!
    print('============>>>>> splitted_festivita_godute')
    print(splitted_festivita_godute)

    retribuzione_settore_values = splitted_retribuzione_settore[1].split(
        '      ')  # re.split(r'             ',retribuzione_settore[1])

    num_max_blank = 7
    i_charcater = -1
    blank_chars = 0

    ore_gg_num = ''
    val_nautra = ''
    competenze = ''
    print('ì****°°°ççééé***°°°')
    print(retricuzione_ordinaria)
    for character in retricuzione_ordinaria[1]:

        i_charcater = i_charcater + 1
        # print('i_charcater: '+str(i_charcater))
        if (character == " "):
            # print('BLANK!!')
            blank_chars += 1
        elif i_charcater < num_max_blank:
            ore_gg_num += character
        elif i_charcater <= num_max_blank * 2:
            # elif i_charcater >= num_max_blank & i_charcater < num_max_blank*2:

            val_nautra += character
        elif i_charcater <= num_max_blank * 4:
            competenze += character

    retribuzione_ordinaria_dict['ore_gg_num'] = ore_gg_num
    retribuzione_ordinaria_dict['val_u_neutra'] = val_nautra
    retribuzione_ordinaria_dict['competenze'] = competenze

    #GRATIFICA NATALIZIA
    num_max_blank = 7
    i_charcater = -1
    blank_chars = 0

    ore_gg_num_natalizia = ''
    val_nautra_natalizia = ''
    competenze_natalizia = ''

    print('@@@@@@-splitted_gratifica_natalizia-@@@@@@@@')
    print(splitted_gratifica_natalizia)
    print('@@@@@@@@@@@@@@@@######|||||||||||||||####@@@@@@@@@@@@@@@@')
    for character in splitted_gratifica_natalizia[1]:

        i_charcater = i_charcater + 1
        # print('i_charcater: '+str(i_charcater))
        if (character == " "):
            # print('BLANK!!')
            blank_chars += 1
        elif i_charcater < num_max_blank:
            ore_gg_num_natalizia += character
        elif i_charcater <= num_max_blank * 2:
            # elif i_charcater >= num_max_blank & i_charcater < num_max_blank*2:

            val_nautra_natalizia += character
        elif i_charcater <= num_max_blank * 4:
            competenze_natalizia += character
    print('===================================')
    gratifica_natalizia_dict["ore_gg_num"] = ore_gg_num_natalizia
    gratifica_natalizia_dict['val_u_neutra'] = val_nautra_natalizia
    gratifica_natalizia_dict['competenze'] = competenze_natalizia

    resultCheck = last_pos_NO_empty(splitted_retribuzione_settore)
    print('...........::::: splitted_retribuzione_settore  :::::::::-......+.....')
    print(splitted_retribuzione_settore)
    print('<><><><><><<<<><<>>>')
    print('resultCheck is :' + str(resultCheck))
    print('### splitted_retribuzione_settore: ' + str(splitted_retribuzione_settore[1]))
    # print('======== '+splitted_retribuzione_settore)
    num_max_blank = 3
    i_charcater = -1
    blank_chars = 0

    num_max_blank = 7
    i_charcater = -1
    blank_chars = 0

    ore_gg_num_settore = ''
    val_nautra_settore = ''
    competenze_settore = ''

    for character in splitted_retribuzione_settore[1:]:

        i_charcater = i_charcater + 1
        # print('i_charcater: '+str(i_charcater))
        if (character == " "):
            # print('BLANK!!')
            blank_chars += 1
        elif i_charcater < num_max_blank:
            ore_gg_num_settore += character
        elif i_charcater <= num_max_blank * 2:
            # elif i_charcater >= num_max_blank & i_charcater < num_max_blank*2:

            val_nautra_settore += character
        elif i_charcater <= num_max_blank * 4:
            competenze_settore += character

    # print(splitted_retribuzione_settore[resultCheck])

    if resultCheck == 3:
        if ore_gg_num_settore != '':
            ore_gg_num_settore = ''
            competenze_settore = splitted_retribuzione_settore[resultCheck]
    elif resultCheck == 2:
        if ore_gg_num_settore != '':
            ore_gg_num_settore = ''
            val_nautra_settore = splitted_retribuzione_settore[resultCheck]

    #FESTIVITA'
    num_max_blank = 3
    i_charcater = -1
    blank_chars = 0

    # print(splitted_festivita_godute)

    num_max_blank = 7
    i_charcater = -1
    blank_chars = 0

    ore_gg_num_festivita = ''
    val_nautra_festivita = ''
    competenze_festivita = ''


    try:
        for character in splitted_festivita_godute[1]:

            i_charcater = i_charcater + 1
            # print('i_charcater: '+str(i_charcater))
            if (character == " "):
                # print('BLANK!!')
                blank_chars += 1
            elif i_charcater < num_max_blank:
                ore_gg_num_festivita += character
            elif i_charcater < num_max_blank * 2:
                val_nautra_festivita += character
            elif i_charcater <= num_max_blank * 4:
                competenze_festivita += character

    except IndexError:
        print('Index problem!!')


    #PUNTO DI TEST

    festivita_godute_dict['ore_gg_num'] = ore_gg_num_festivita
    festivita_godute_dict['val_nautra'] = val_nautra_festivita
    festivita_godute_dict['competenze'] = competenze_festivita



    buoni_pasto_values = buoni_pasto[1].split()
    buoni_pasto_values
    #doxooxoxooxoxoxooxo
    num_max_blank = 7  # 10
    i_charcater = -1
    blank_chars = 0
    ore_gg_num_pasto = ''
    val_nautra_pasto = ''
    competenze_pasto = ''
    print('buoni pasto vals: ')
    print(buoni_pasto_values)
    ore_gg_num_pasto = buoni_pasto_values[0]
    val_nautra_pasto = buoni_pasto_values[1]
    competenze_pasto = buoni_pasto_values[2]

    buoni_pasto_dict["ore_gg_num"] = ore_gg_num_pasto
    buoni_pasto_dict["val_u_neutra"] = val_nautra_pasto
    buoni_pasto_dict['competenze'] = competenze_pasto
    #VALORE TOTALE
    text_imponibile_fpld = text.split("\n")[25]
    print('text_imponibile_fpld linea di testo =================:')
    print(text_imponibile_fpld)
    resultVal = re.search("F.P.L.D.\s+([\d,]+).\s+IMP.", text_imponibile_fpld)
    #print('testo da analizzare: '+text_imponibile_fpld)
    print('TYPE OF resultVal -----------------------------------')
    print(type(resultVal))
    print('TYPE OF resultVal -----------------------------------')

    try:
        contrib_valori_dict["imponibile_fpld"] = resultVal.group(1)
    except(AttributeError):
        contrib_valori_dict["imponibile_fpld"] = 'xxxxxxxx'

    text_ctr_fpld = text.split("\n")[26]
    print('@#@#@#@#@#@#@#@#@#@#@#')
    print(text_ctr_fpld)
    print('@#@#@#@#@#@#@#@#@#@#@#')
    resultVal_ctr_fpld = re.search("F.P.L.D.\s+([\d,]+).\s+CT R.", text_ctr_fpld)
    print('testo analizzare:-----')
    print(text_ctr_fpld)
    print('testo analizzare:-----')
    ##if resultVal_ctr_fpld:
    ##    print('REG EXPRESSION GOT VALUE @#@#@#@#@#@#@#@')


    try:
        contrib_valori_dict["ctr_fpld"] = resultVal_ctr_fpld.group(1)
    except(AttributeError):
        contrib_valori_dict["ctr_fpld"] = 'xxxxxxxx'

    text_ctr_cigs = text.split("\n")[27]
    #print('TESTO text_ctr_cigs è:::::88888888===88888888888888:::::::::')
    #print(text_ctr_cigs)
    resultVal_ctr_cigs = re.search("C.I.G.S.\s+([\d,]+).\s+CTR", text_ctr_cigs)
    #print(resultVal_ctr_cigs.group(1))
    try:
        contrib_valori_dict["ctr_cigs"] = resultVal_ctr_cigs.group(1)
    except(AttributeError):
        contrib_valori_dict["ctr_cigs"] = 'xxxxxxxx'
    text_ctr_cigs = text.split("\n")[53]

    size_value = len('          ')

    print(text_ctr_cigs)

    ctr_cigs_splitted = [text_ctr_cigs[i:i + size_value] for i in range(0, len(text_ctr_cigs), size_value)]
    try:
        progr_dict["imp_inail"] = ctr_cigs_splitted[0]
    except IndexError:
        progr_dict["imp_inail"] = '   '
    try:
        progr_dict["imp_contr"] = ctr_cigs_splitted[1]
    except IndexError:
        progr_dict["imp_contr"] = '   '
    try:
        progr_dict["contributi"] = ctr_cigs_splitted[2]
    except IndexError:
        progr_dict["contributi"] = '   '
    try:
        progr_dict["reddito_compl"] = ctr_cigs_splitted[3]
    except IndexError:
        progr_dict["reddito_compl"] = '   '
    try:
        progr_dict["oneri_deducibili"] = ctr_cigs_splitted[4]
    except IndexError:
        progr_dict["oneri_deducibili"] = '   '
    try:
        progr_dict["imp_irpef"] = ctr_cigs_splitted[5]
    except IndexError:
        progr_dict["imp_irpef"] = '   '
    try:
        progr_dict["detrazioni_lavoro_dip"] = ctr_cigs_splitted[6]
    except IndexError:
        progr_dict["detrazioni_lavoro_dip"] = '   '
    try:
        progr_dict["detrazioni_familiari"] = ctr_cigs_splitted[7]
    except IndexError:
        progr_dict["detrazioni_familiari"] = '   '
    testo_temp = text.split("\n")[26]
    totale_competenze = text.split("\n")[26]
    totale_competenze = totale_competenze[70:]
    print('XXXXXXXXXXXX__totale_competenze==____=XXXXXXXXXXXXXXX: '+str(testo_temp))

    totale_competenze = re.search(r"\s+([\d,]+)", totale_competenze)
    try:
        valori_TOTALE_dict["TOTALE COMPETENZE"] = totale_competenze.group(1)
    except(AttributeError):
        valori_TOTALE_dict["TOTALE COMPETENZE"] = 'xxxxxxxx'



    totale_trattenute = text.split("\n")[28]
    totale_trattenute = re.search(r"DE TR A ZIO N I\s+[\d,]*\s+([\d,])*", totale_trattenute)
    # print(totale_trattenute.group(1)) totale_trattenute.group(1)
    try:
        if (totale_trattenute.group(1) is not None):
            valori_TOTALE_dict["TOTALE TRATTENUTE"] = totale_trattenute.group(1)
        else:
            valori_TOTALE_dict["TOTALE TRATTENUTE"] = '      '
    except(AttributeError):
        valori_TOTALE_dict["TOTALE TRATTENUTE"] = 'xxxxxxxx'

    totale_tratt_sociali = re.search(r"TR AT TE N UT A A R RE TR .A .P. \s+[\d,]*\s+([\d,]*)", text_imponibile_tratt_ap)

    text_imponibile_tratt_ap
    ############if (totale_tratt_sociali.group(1) is not None):
    try:
        valori_TOTALE_dict['TOTALE TRATT.SOCIALI'] = totale_tratt_sociali.group(1)
    except(AttributeError):
        valori_TOTALE_dict['TOTALE TRATT.SOCIALI'] = 'xxxxxxxx'


    print('text_add_comunale UUUUUUUUUUUUUUUU')
    print(text_add_comunale)

    totale_tratt_fiscale = re.search(r"AD D. CO M UN AL E  AC CO N TO \s+[\d,]*\s+([\d,]*)", text_add_comunale)
    ###########totale_tratt_fiscale.group(1)


    try:
        if (totale_tratt_fiscale.group(1) is not None):
            valori_TOTALE_dict["TOTALE TRATT.FISCALE"] = totale_tratt_fiscale.group(1)
        else:
            valori_TOTALE_dict["TOTALE TRATT.FISCALE"] = '      '
    except AttributeError:
        valori_TOTALE_dict["TOTALE TRATT.FISCALE"] = '      '


    conguaglio_irpef = re.search(r"RIT EN U TA  IR PE F  PA GA TA \s+[\d,]*\s+([\d,]*)", text_irpes_annuo_pagata)
    try:
     if (conguaglio_irpef.group(1) is not None):
        valori_TOTALE_dict['CONGUAGLIO IRPEF'] = conguaglio_irpef.group(1)
     else:
        valori_TOTALE_dict['CONGUAGLIO IRPEF'] = '      '

    except AttributeError:
        valori_TOTALE_dict['CONGUAGLIO IRPEF'] = '      '

    arrotond_preced = re.search(r"GG .D ET RA ZI ON I\s+[\d,]*\s+([\d,]*)", text_arrotond_preced)

    try:
        valori_TOTALE_dict['ARROTOND.PRECEND'] = arrotond_preced.group(1)
    except(AttributeError):
        valori_TOTALE_dict['ARROTOND.PRECEND'] = 'xxxxxxxx'

    #if (arrotond_preced.group(1) is not None):

    #else:
    #    valori_TOTALE_dict['ARROTOND.PRECEND'] = '      '

    arrotond_attuale = re.search(r"P AR T -TI ME\s+[\d,]*\s+([\d,]*)", text_arrotond_attuale)
    #print('valore della variabile arrotond_preced:')
    #print(arrotond_preced)
    try:
        if (arrotond_attuale.group(1) is not None):
            valori_TOTALE_dict['ARROTOND.ATTUALE'] = arrotond_attuale.group(1)
        else:
            valori_TOTALE_dict['ARROTOND.ATTUALE'] = '       '
    except AttributeError:
        valori_TOTALE_dict['ARROTOND.ATTUALE'] = '       '

    return jsonify(company_name=citPaycheck["COMPANY"],
                   company_address=citPaycheck['COMPANY_ADDRESS'],
                   TAX_CODE_COMPANY=citPaycheck['TAX_CODE_COMPANY'],
                   EMPLOYEE=citPaycheck['EMPLOYEE'],
                   EMPLOYEE_ADDRESS=citPaycheck['EMPLOYEE_ADDRESS'],
                   TAX_CODE_EMPLOYEE=citPaycheck["TAX_CODE_EMPLOYEE"],
                   TOT_SALARY=citPaycheck['TOT_SALARY'],
                   RETR_ORDINARIA_ore_gg_num=retribuzione_ordinaria_dict['ore_gg_num'],
                   RETR_ORDINARIA_val_u_neutra=retribuzione_ordinaria_dict['val_u_neutra'],
                   RETR_ORDINARIA_competenze=retribuzione_ordinaria_dict['competenze'],
                   GRAT_NATAL_ore_gg_num=gratifica_natalizia_dict['ore_gg_num'],
                   GRAT_NATAL_val_u_neutra=gratifica_natalizia_dict['val_u_neutra'],
                   GRAT_NATAL_competenze=gratifica_natalizia_dict['competenze'],
                   RETR_SETT_ore_gg_num=retributivo_settore_dict['ore_gg_num'],
                   RETR_SETT_val_u_neutra=retributivo_settore_dict['val_u_neutra'],
                   RETR_SETT_competenze=retributivo_settore_dict['competenze'],
                   FEST_GODUTE_ore_gg_num=festivita_godute_dict['ore_gg_num'],
                   FEST_GODUTE_val_u_neutra=festivita_godute_dict['val_u_neutra'],
                   FEST_GODUTE_competenze=festivita_godute_dict['competenze'],
                   BUONI_PASTO_ore_gg_num=buoni_pasto_dict['ore_gg_num'],
                   BUONI_PASTO_val_u_neutra=buoni_pasto_dict['val_u_neutra'],
                   BUONI_PASTO_competenze=buoni_pasto_dict['competenze'],
                   INFO_IRPEF_LORDA=info_fiscale_dict['imponibile_irpef_lorda'],
                   INFO_TAS_SEPARATA=info_fiscale_dict['imp_tas_separata'],
                   INFO_IMP_ARR_AP=info_fiscale_dict['imponibile_arretrati_ap'],
                   INFO_REGIO_AP=info_fiscale_dict['add_regionale_ap'],
                   INFO_COMUN_AP=info_fiscale_dict['add_comunale_ap'],
                   INFO_TRAT_IRPEF_LORDA=info_fiscale_dict['trattenuta_irpef_lorda'],
                   INFO_TRAT_TAS_SEP=info_fiscale_dict['trattenuta_tas_separata'],
                   INFO_ALIQ_MEDIA_AP=info_fiscale_dict['aliquota_media_ap'],
                   INFO_ADD_REG_AC=info_fiscale_dict['add_regionale_ac'],
                   INFO_ADD_COMUN_AC=info_fiscale_dict['add_comunale_ac'],
                   INFO_DETRAZIONI=info_fiscale_dict['detrazioni'],
                   INFO_TRAT_IRPEF_NETTA=info_fiscale_dict['trattenuta_irpef_netta'],
                   INFO_TRAT_ARR_AP=info_fiscale_dict['trattenuta_arretr_ap'],
                   INFO_ADD_COMUN_ACCONTO=info_fiscale_dict['add_comunale_acconto'],
                   VALORI_TOT_COMP=valori_TOTALE_dict['TOTALE COMPETENZE'],
                   VALORI_TOT_TRATT=valori_TOTALE_dict['TOTALE TRATTENUTE'],
                   VALORI_TOT_TRATT_SOC=valori_TOTALE_dict['TOTALE TRATT.SOCIALI'],
                   VALORI_TOT_TRATT_FISC=valori_TOTALE_dict['TOTALE TRATT.FISCALE'],
                   VALORI_TOT_CONG_IRPEF=valori_TOTALE_dict['CONGUAGLIO IRPEF'],
                   VALORI_TOT_PRECEND=valori_TOTALE_dict['ARROTOND.PRECEND'],
                   VALORI_TOT_ATTUALE=valori_TOTALE_dict['ARROTOND.ATTUALE'],
                   CONTRIB_VALORI_IMPLE_FPLD=contrib_valori_dict['imponibile_fpld'],
                   CONTRIB_VALORI_CTR_FPLD=contrib_valori_dict['ctr_fpld'],
                   CONTRIB_VALORI_CTR_CIGS=contrib_valori_dict['ctr_cigs'],
                   PROGR_DICT_IMP_INAIL=progr_dict['imp_inail'],
                   PROGR_DICT_IMP_CNTR=progr_dict['imp_contr'],
                   PROGR_DICT_CONTRIBUTI=progr_dict['contributi'],
                   PROGR_DICT_REDD_COMPL=progr_dict['reddito_compl'],
                   PROGR_DICT_ONE_DEDU=progr_dict['oneri_deducibili'],
                   PROGR_DICT_IMP_IRPEF=progr_dict['imp_irpef'],
                   PROGR_DICT_DETR_LAV_DIP=progr_dict['detrazioni_lavoro_dip'],
                   PROGR_DICT_DETR_FAM=progr_dict['detrazioni_familiari']
                   )











'''
###
text=''
if pdfFileName != '':
    with pdfplumber.open(pdfFileName) as pdf:
        main_page = pdf.pages[0]
        text = main_page.extract_text()
        #page = main_page.extract_table()
else:
    pdfFileName = 'BUSTA_PAGA_AM.pdf'


def has_numbers(inputString):
    x = re.search("re.search(r'\d+')", inputString)
    if x:
        #print("YES! are numbers")
        res = True
    else:
        res = False
    return res


lunghezza_3_slot = 66
lunghezza_1_slot = 46

#print('###***####')
#print(text.split("\n"))

pattern_nome_ditta = '                                  '
pattern_indirizzo_ditta = re.compile('                 ')
pattern_dipendente = re.compile('D IPE N DE NT E(.+)ELEMENTI DELLA RETRIBUZIONE')
text_nome_ditta = text.split("\n")[0]
text_cod_fiscale_ditta = text.split("\n")[3]   #error out of range
text_dipendente = text.split("\n")[4]
text_indirizzo_dipendente = text.split("\n")[5]
text_indirizzo_citta = text.split("\n")[1]  # -----
text_cod_fiscale_dipendente = text.split("\n")[8]
text_totale_stipendio = text.split("\n")[17]

text_retribuzione_ordinaria = text.split("\n")[18]  # fatto
# print('text_retribuzione_ordinaria')
# print(text_retribuzione_ordinaria)
text_gratifica_natalizia = text.split("\n")[19]  ##@@@@@
text_retributivo_settore = text.split("\n")[20]  # devo andare una roga su
text_festivita_godute = text.split("\n")[22]
text_buoni_pasto = text.split("\n")[23]
#print('festivita_godute')
#print(text_festivita_godute)
text_irpef_lorda_detrazioni_1 = text.split("\n")[28]
text_tassazione_separata_irpef_netta = text.split("\n")[29]  # çççççççççççççççççççççççççççççç
text_imponibile_tratt_ap = text.split("\n")[30]
text_add_regionale_ap_ac = text.split("\n")[32]  # [31]
text_irpes_annuo_pagata = text.split("\n")[35]
text_arrotond_preced = text.split("\n")[38]
text_arrotond_attuale = text.split("\n")[40]
# print('Riga add_regionale_ap:')
# print(text_add_regionale_ap_ac)
add_regio_ap = text_add_regionale_ap_ac[34:45]
#print('add_regio_ap:')
#print(add_regio_ap)

add_regio_ac = text_add_regionale_ap_ac[63:64]
#print('add_regio_ac')
#print(add_regio_ac)
text_add_comunale = text.split("\n")[33]
add_comu_ap = text_add_comunale[5:25]
#print('==oo==oo==oo==')
#print(text_add_comunale[4:43])  # [3:25]

if (has_numbers(add_regio_ap)):
    info_fiscale["add_regionale_ap"] = add_regio_ap
else:
    info_fiscale["add_regionale_ap"] = '        '
if (has_numbers(add_regio_ac)):
    info_fiscale["add_regionale_ac"] = add_regio_ac
else:
    info_fiscale["add_regionale_ac"] = '        '

text = text_add_comunale.split()

#print('ççççç@@@@ ')
# print(text_add_comunale)
x = re.search("(.{25})(\s*|\d+)(0-9)*(\s+|\d+)", text_add_comunale).group(4)  ########## ACCONTO(\.+')\S+
#print('ACCONTO: ' + x)  #
if (text[2].isnumeric()):  # ADD.COMUNALE A.P
    info_fiscale['add_comunale_ap'] = text[2]
else:
    info_fiscale['add_comunale_ap'] = '     '

if (text[5].isnumeric()):  # ADD.COMUNALE A.C
    info_fiscale['add_comunale_ac'] = text[5]
else:
    info_fiscale['add_comunale_ac'] = '     '

imp_arr_ap = text_imponibile_tratt_ap[37:45]
aliquota_media_ap = text_imponibile_tratt_ap[78:88]
trattenuta_arretr_ap = text_imponibile_tratt_ap[122:130]  # ===================================
if has_numbers(trattenuta_arretr_ap):
    info_fiscale["trattenuta_arretr_ap"] = trattenuta_arretr_ap
else:
    info_fiscale["trattenuta_arretr_ap"] = '        '

#print('- trattenuta_arretr_ap' + trattenuta_arretr_ap)
info_fiscale["aliquota_media_ap"] = aliquota_media_ap

###########aliquota_media_ap = re.search('AAA(.+?)ZZZ', text).group(1)
# print('lunghezza retribuzione_ordinaria text: '+str(len(text_retribuzione_ordinaria)))    ##info_fiscale = {
# print('11--111 '+text_irpef_lorda_detrazioni_1)
#print('22--222 ' + text_tassazione_separata_irpef_netta)
irpef_lorda__1 = text_irpef_lorda_detrazioni_1[0:45]
irpef_lorda__1 = irpef_lorda__1.split('\d+')[0]
irpef_lorda__1 = re.split('   ', irpef_lorda__1)

tassazione_irpef_lorda_1 = text_irpef_lorda_detrazioni_1[50:90]
tassazione_irpef_lorda_1 = tassazione_irpef_lorda_1.split('\d+')[0]
tassazione_irpef_lorda_1 = re.split('   ', tassazione_irpef_lorda_1)
detrazioni_1 = text_irpef_lorda_detrazioni_1[115:]

imp_tass_separata = text_tassazione_separata_irpef_netta[0:45]
tratt_tassaz_separata = text_tassazione_separata_irpef_netta[48:90]
tass_separata_irpef_netta = text_tassazione_separata_irpef_netta[90:135]  #################
#   tratt_irpef_netta

if (has_numbers(tratt_tassaz_separata)):
    tratt_tassaz_separata = re.split(r'\d+', tratt_tassaz_separata)
else:
    tratt_tassaz_separata = ' '

info_fiscale["trattenuta_tas_separata"] = tratt_tassaz_separata
# print('splitting tratt_tassaz_separata')
#print(tratt_tassaz_separata)

info_fiscale["imp_tas_separata"] = tass_separata_irpef_netta[3]
# print('tratt_tassaz_separata')
# print(tratt_tassaz_separata)

trattenuta_irpef_lorda_1 = text_irpef_lorda_detrazioni_1[50:90]
trattenuta_irpef_lorda_1 = trattenuta_irpef_lorda_1.split('\d+')[0]
trattenuta_irpef_lorda_1 = re.split('   ', trattenuta_irpef_lorda_1)

detrazioni_1 = text_irpef_lorda_detrazioni_1[115:]
# print('tratt_irpef_netta')
tratt_irpef_netta = re.split('   ', tass_separata_irpef_netta)
tratt_irpef_netta = tratt_irpef_netta[1]
#print('tratt_irpef_netta ====')
#print(tratt_irpef_netta)
info_fiscale["trattenuta_irpef_netta"] = tratt_irpef_netta
info_fiscale['"imponibile_arretrati_ap"'] = imp_arr_ap

#################################tassaz.separata-irpef_netta = text_tassazione_separata_irpef_netta[]
info_fiscale['trattenuta_irpef_lorda'] = trattenuta_irpef_lorda_1[1]
info_fiscale["imponibile_irpef_lorda"] = irpef_lorda__1[2]
info_fiscale['detrazioni'] = detrazioni_1

pattern_indirizzo_dipendente = 'Minimo'
nome_deipendente = re.split(pattern_dipendente, text_dipendente)
indirizzo_ditta = re.split(pattern_indirizzo_ditta, text_indirizzo_citta)
indirizzo_dipendente = re.split(pattern_indirizzo_dipendente, text_indirizzo_dipendente)[0]
alpha_indirizzo_dipendente = re.sub(r'[^a-zA-Z]', '', indirizzo_dipendente)
codice_fiscale_ditta = re.split(r'POSIZIONE INPS', text_cod_fiscale_ditta)  # r'[^a-zA-Z]', '', indirizzo_dipendente
codice_fiscale_dipendente = text_cod_fiscale_dipendente[26:]
buoni_pasto = text_buoni_pasto.split(r'              ')
#print('text_buoni_pasto   ======>')
#print(buoni_pasto[0])

citPaycheck["COMPANY"] = re.split(pattern_nome_ditta, text_nome_ditta)[0]

citPaycheck["COMPANY_ADDRESS"] = re.split(pattern_indirizzo_ditta, text_indirizzo_citta)
citPaycheck["TAX_CODE_COMPANY"] = re.split(r'POSIZIONE INPS', text_cod_fiscale_ditta)
citPaycheck["EMPLOYEE"] = re.split(pattern_dipendente, text_dipendente)
citPaycheck["EMPLOYEE_ADDRESS"] = re.sub(r'[^a-zA-Z]', '', indirizzo_dipendente)
citPaycheck["TAX_CODE_EMPLOYEE"] = text_cod_fiscale_dipendente[26:]
citPaycheck["TOT_SALARY"] = re.findall('[\d]{4}[?\,][\d]+', text_totale_stipendio)  # [0]

# print('**add_comunale_ap:**'+text_add_comunale)

splitted_codice_fiscale_ditta = codice_fiscale_ditta[0].split('  ')
for i in splitted_codice_fiscale_ditta:
    if (len(i) == 11 and i.isnumeric()):
        print('codice fiscale ditta: ' + i)

stipendio = re.findall('[\d]{4}[?\,][\d]+', text_totale_stipendio)
# print('text_totale_stipendio: '+text_totale_stipendio)
#print('LUNGHEZZA stipendio: ' + str(len(stipendio)))

#merge all dictionares
dict_result = citPaycheck.copy()
for key, value in retribuzione_ordinaria.items():
    key_val = 'retr_ord_'+key
    dict_result[key_val] = value

for key, value in gratifica_natalizia.items():
    key_val = 'gra_natal_'+key
    dict_result[key_val] = value

for key, value in retributivo_settore.items():
    key_val = 'retr_set_'+key
    dict_result[key_val] = value

for key, value in festivita_godute.items():
    key_val = 'fest_god_' + key
    dict_result[key_val] = value

for key, value in buoni_pasto_dict.items():
    key_val = 'buoni_' + key
    dict_result[key_val] = value

for key, value in info_fiscale.items():
    key_val = key
    dict_result[key_val] = value

for key, value in valori_TOTALE.items():
    key_val = key
    dict_result[key_val] = value

for key, value in contrib_valori.items():
    key_val = key
    dict_result[key_val] = value

for key, value in progr_dict.items():
    key_val = key
    dict_result[key_val] = value


'''
###app_json = json.dumps(dict_result)
###########flask.jsonify(**dict_result)
if __name__ == "__main__":
    app.run(host="localhost", port=8000, debug=True)
  